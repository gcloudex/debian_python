# debian_python

experiment with Gitlab CI/CD for building docker image

## Overcoming login warning

Docker login stores credential unencrypted.
```
$ echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

Solution for your CI runner:
- https://hackernoon.com/getting-rid-of-docker-plain-text-credentials-88309e07640d
  
What does it mean for Gitlab share runner?
See https://forum.gitlab.com/t/gitlab-ci-docker-template-credentials/23373 


## References

- https://www.youtube.com/watch?v=Jav4vbUrqII

- https://dev.to/imichael/automagically-build-and-push-docker-images-to-a-registry-using-gitlab-276p

- https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/

- https://hackernoon.com/getting-rid-of-docker-plain-text-credentials-88309e07640d

- https://gitlab.com/help/ci/yaml/README.md

- Environment Variables
  - https://docs.gitlab.com/ee/ci/variables/README.html
  - https://docs.gitlab.com/ee/ci/variables/predefined_variables.html

